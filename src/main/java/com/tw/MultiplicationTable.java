package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(!isValid(start, end)){
            return null;
        }
        return generateTable(start,end);
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start)&&isInRange(end)&&isStartNotBiggerThanEnd(start,end);
    }

    public Boolean isInRange(int number) {
        return number<=1000&&number>0;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start<=end;
    }

    public String generateTable(int start, int end) {
        StringBuilder generatedTable= new StringBuilder();
        for(int i=start;i<=end;i++){
            generatedTable.append(generateLine(start,i));
            if(i<end){
                generatedTable.append("%n");
            }
        }
        return String.format(generatedTable.toString());
    }

    public String generateLine(int start, int row) {
        StringBuilder generatedLine= new StringBuilder();
        for(int i=start;i<=row;i++){
            generatedLine.append(generateSingleExpression(i, row));
            if(i<row) {
                generatedLine.append("  ");
            }
        }
        return generatedLine.toString();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        return String.valueOf(multiplicand) +
                '*' +
                multiplier +
                '=' +
                multiplicand * multiplier;
    }
}
